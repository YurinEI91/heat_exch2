#pragma once

unsigned k;
namespace pipe {
	double const RE_LAMINAR = 2100.0;
	double const RE_TURBULENT = 2300.0;
	namespace round {
		namespace metal {
			double Nu(double Pe) {
				if (Pe < 4000)
					return 5 + 0.025 * pow(Pe, 0.8);//kirillov p.116 (7.6) 
				return 7.5 + 0.005 * Pe;//kirillov p.116 (7.7) 
			}
		}
		namespace laminar {
			double Nu() {
				return 3.66;
			}
			double xi(double Re) {//   Kirillov    p.28  (1.7)
				return 64.0 / Re;
			}
		}
		namespace turbulent {
			double xi(double Re) {//   Kirillov    p.29    (1.10)
				double xi = 1.82*log10(Re) - 1.64;
				return 1.0 / xi / xi;
			}
			double Nu(double Re, double Pr) {//   Kirillov    p.69  (4.1)   +-10%
				double k = xi(Re);
				return k / 8.0*Re*Pr / (1.0 + 900.0 / Re + 4.5 * sqrt(k)*(pow(Pr, 2.0 / 3.0) - 1.0));
			}
			namespace rough {
				//double const A = 0.0016987;
				//double xi(double Re,double d2)
				//return 0.11*pow(A / d2 + 68 / Re, 0.25);*/
				//todo
			}
		}
		double Nu(double Re, double Pr);

		namespace boiling {// H2O only todo
			const unsigned DIFENCE = 1000;
			namespace regular {
				double alpha_0(double q, double p) {
					return 4.34 * pow(q, 0.7) * (pow(p, 0.14) + 1.35e-2 * p * p);//   Kirillov    p.84  (5.2) done
				}

				double alpha_1(double alph_c, double alph_0) {
					return sqrt(alph_c * alph_c + 0.49 * alph_0 * alph_0);//   Kirillov    p.86  (5.15)  done
				}

				double alpha(double alph_c, double p, double q, double Re, double Pr, double rho_l, double w_mix, double l) {
					double alph_0 = alpha_0(q, p);
					//double alph_c = turbulent::Nu(Re, Pr); // done
					//if (alph_0 < 0.5*alph_c)
					//	return alph_c;
					//if (alph_0 > 3 * alph_c)
					//	return 0.9*alph_0;
					//return alph_c * sqrt(1+pow(0.9*alph_0/alph_c,2));
					double alph_1 = alpha_1(alph_c, alph_0);
					return alph_1 * sqrt(1 + 7.0e-9 * pow(l * w_mix * rho_l / q, 1.5) * pow(0.7 * alph_0 / alph_1, 2));//   Kirillov    p.86  (5.15) done
				}
				double alpha(double alph_c, double T, double Tw, double p, double Re, double Pr, double rho_l, double w_circ, double l) {
					double alpha = 50.0;
					double alpha_ = 0.0;					
					double q = alpha * (Tw - T);
					for (int i = 0; i < DIFENCE; i++) {
						alpha_ = alpha;
						alpha = regular::alpha(alph_c,p, q, Re, Pr, rho_l, w_circ, l);
						q = 0.5*q + 0.5*alpha*(Tw - T);
						if (abs(alpha - alpha_)/alpha < 0.05)
							break;
					}
					/*if (alpha > 100000)
						return 100000;*/
					return alpha;
				}
			}
			namespace crisis {
				double Nu(double Pe, double rho_v, double rho_l, double x) {

					double Y = 1.0;
					if (rho_l <= 450 * rho_v)
						Y = 1 + 0.5*(1 - x)*pow(rho_l / rho_v - 1, 0.8);
					else
						Y = 1 + 70 * (1 - x);
					//double Y = 1 - 0.1 * pow(rho_l / rho_v - 1, 0.4) * pow(1 - x, 0.4);//   Kirillov    p.87  (5.22) done
					return 0.017* Y* pow(Pe * (x + (rho_v / rho_l) * (1 - x)), 0.8);//   Kirillov    p.87  (5.23) done
					//return 0.017*(1 + 3.18* ? ? )*pow(Pe,0.8)*Y;
				}
			}
			double sigma(double T) {
				double tau = 1 - T / 647.096;
				return 235.8e-3* pow(tau, 1.256)* (1 - 0.625 * tau);
			}
			double x_cr(double P, double h, double w_mix, double D, Properties *prop, double T) {
				return 1 - 0.86 * exp(-19 / (prop->density(P, h) * w_mix * sqrt(D / prop->rho_l(P) / sigma(T))));//   Kirillov    p.97  (6.15) done
				//return 0.4;
			}
            double alpha(double Re, double Pr, double P, double h, double T, double Tw, double G, double s, double D, Properties* prop, double Re_vap, double Pr_vap) {
				double rho_l = prop->rho_l(P);
				double rho_v = prop->rho_v(P);
				double w_mix = abs(G) / s / prop->density(P, h);
				double w_circ = abs(G) / s / rho_l;
				double x = prop->x(P, h);
				double alph_c = Nu(w_circ*D*rho_l/prop->viscos_liq(P), prop->Pr_liq(P))*prop->lam_liq(P)/D;
				//double w_mix2 = w_circ * (1+x*(rho_l/rho_v-1));
				double alpha_b = regular::alpha(alph_c, T, Tw, P, Re, Pr, rho_l, w_circ, prop->h_l_v(P));
				double xcr = x_cr(P, h, w_mix, D, prop, T) - 0.1;
				if (xcr < 0)
					xcr = 0;
                if (x < xcr)
                    return alpha_b;
				double alpha_cr = crisis::Nu(Re_vap * Pr_vap, rho_v, rho_l, x)*prop->lam_vap(P) / D;

				if (alpha_cr > alpha_b)
					return alpha_b;
				if (x - xcr < 0.2)
					return (alpha_b*(0.2 - (x - xcr)) + alpha_cr * (x - xcr)) / 0.2;
				return alpha_cr;
            }
		}
		double Nu(double Re, double Pr) {
			if (Re < RE_LAMINAR)
				return laminar::Nu();
			if (Re > RE_TURBULENT)
				return turbulent::Nu(Re, Pr);
			double Nu_l = laminar::Nu();
			double Nu_t = turbulent::Nu(RE_TURBULENT, Pr);
			return Nu_l + (Re - RE_LAMINAR)*(Nu_t - Nu_l) / (RE_TURBULENT - RE_LAMINAR);
		}

		double xi(double Re) {
			if (Re < RE_LAMINAR)
				return laminar::xi(Re);
			if (Re > RE_TURBULENT)
				return turbulent::xi(Re);
			double xi_l = laminar::xi(RE_LAMINAR);
			double xi_t = turbulent::xi(RE_TURBULENT);
			return xi_l + (Re - RE_LAMINAR)*(xi_t - xi_l) / (RE_TURBULENT - RE_LAMINAR);
		}
	}
	namespace ring {
		namespace metall {
			double Nu(double Re, double Pr, double tet) {
				return (6.4 - 3.0 / log10(Re)) * pow(1/tet, 0.24) + 0.008*pow(Re * Pr, 0.87)*(1.0 + 0.5 * exp(-4 * tet));
			}
		}
		namespace boiling {

		}
		namespace laminar {
			double Nu() {//?! todo
				return 3.66;
			}
			double xi(double Re, double theta) {//   Kirillov    p.28  (1.7)
				double A = (1 - theta * theta) / (1 + theta * theta + (1 - theta * theta) / log(theta));
				return 64.0*A / Re;
			}
		}
		namespace turbulent {
			double xi(double Re, double theta) {//   Kirillov    p.30  (1.13)
				double A = (1 - theta) / (1 + (1 - theta * theta) / log(theta*theta));
				return 64.0* pow(A, 0.62)*(1 + 0.04*theta) / Re;;
			}
			double Nu(double Re, double Pr, double theta) {//   Kirillov    p.69  (4.1)   +-10%
				double k = xi(Re, theta);
				return k / 8.0*Re*Pr / (1.0 + 900.0 / Re + 4.5 * sqrt(k)*(pow(Pr, 2.0 / 3.0) - 1.0));
			}
			namespace rough {
				//double const A = 0.0016987;
				//double ksi(double Re,double d2)
				//return 0.11*pow(A / d2 + 68 / Re, 0.25);*/
				//todo
			}
		}
		double Nu(double Re, double Pr, double theta) {
			if (Re < RE_LAMINAR)
				return laminar::Nu();
			if (Re > RE_TURBULENT)
				return turbulent::Nu(Re, Pr, theta);
			double Nu_l = laminar::Nu();
			double Nu_t = turbulent::Nu(RE_TURBULENT, Pr, theta);
			return Nu_l + (Re - RE_LAMINAR)*(Nu_t - Nu_l) / (RE_TURBULENT - RE_LAMINAR);
		}
		double xi(double Re, double theta) {
			if (Re < RE_LAMINAR)
				return laminar::xi(Re, theta);
			if (Re > RE_TURBULENT)
				return turbulent::xi(Re, theta);
			double xi_l = laminar::xi(RE_LAMINAR, theta);
			double xi_t = turbulent::xi(RE_TURBULENT, theta);
			return xi_l + (Re - RE_LAMINAR)*(xi_t - xi_l) / (RE_TURBULENT - RE_LAMINAR);
		}
	}
}
namespace pucki {
	double Nu(double Pe,double step, double phi) {
        phi *= pi/180.0;
		double Nu;
		if(!phi) //prodol'noe obtekanie
			Nu = 7.55*step - 20.0/pow(step, 13) + 0.041/step/step * pow(Pe, 0.56 + 0.19*step);
		else
			Nu = 2*sqrt(Pe)*pow(sin(phi), 0.4);// Kirillov p. 123 (7.52)
		if (Nu > 1 && Pe > 1)
			return Nu;
		return 1;
        //return 2*sqrt(Pe)* sqrt(sin(phi * pi / 180) + sin(phi * pi / 180) * sin(phi * pi / 180)) / sqrt(1 + sin(phi * pi / 180) * sin(phi * pi / 180)); //kirillov p.123 (7.53)
	}

}



