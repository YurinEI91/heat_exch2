#pragma once
ofstream hlop("hlop.txt");
struct HeatStructure; //predestination
using namespace std;
class Channel {
public:
	bool IsItMetal;
	string name;
	double L;
	double *G0;
	vector<double> Q;
	vector<double> alpha; //heat transfer coefficient
	vector<double> T;
	vector<double> P;
	vector<double> P_;
	vector<double> h;
	vector<double> h_;
	vector<double> G1;
	vector<double> G2;
	vector<double>deltaP;
	vector <dvumer*> melt;	
	double shag;
	double qx, qx_;
	struct Geometry {// Control Volume (CV) geometry
		double s;   //CV passage area
		double dx;  //CV length
		double d1;  //inlet diameter
		double d2;  //outlet diameter
		double D;   //hydraulic diameter
		double dh;  //height difference
		double phi;
		double Dvn;
		double *Twall;
	};
	vector <Geometry> geom;
	Properties properties;

private:

	//Boundary conditions
	struct t_val {
		double time;
		double value;
		t_val(double t, double v) {
			time = t;
			value = v;
		}
	};
	vector<t_val> BoundH1;
	vector<t_val> BoundH2;
	vector<t_val> BoundG;
	vector<t_val> BoundPin;
	vector<t_val> BoundP;
	void rashod() {
		bool roundp = false;
		double ksi = 0.02;
		double Re;
		double deltaP;
		double dp;
		double dG;
		int k=0;
		double pvuh = P[G2.size() - 1];
		if (geom[1].d2 > 0 && geom[1].d1 == 0)
			roundp = true;
		for (int count = 0; count < 500; count++) {
			G2[0] = geom[1].s * sqrt((2 * (P[0] - P[P.size() - 1]) * properties.density(P[0], h[0]) * (geom[1].d2 - geom[1].d1)) / (ksi * L));
			Re = 4 * abs(G2[0]) / (pi * (geom[1].d2 - geom[1].d1) * properties.viscosity(P[0], h[0]));
			if (roundp)
				ksi = pipe::round::xi(Re);
			else
				ksi = pipe::ring::xi(Re, geom[0].d1 / geom[0].d2);
		}
		do {
			for (int i = 1; i < (int)G1.size(); i++) {
				G1[i] = -G2[i - 1];
				G2[i] = - G1[i];
			}
			for (int i = 1; i < G1.size(); i++) {
				Re = 4 * abs(G1[i]) / (pi * (geom[i].d2 - geom[i].d1) * properties.viscosity(P[i], h[i]));

				if (geom[i].d2 > 0 && geom[i].d1 == 0) 
					ksi = pipe::round::xi(Re);
				
				if (geom[i].d1 > 0 && geom[i].d2 > 0 && geom[i].d2 > geom[i].d1)
					ksi = pipe::ring::xi(Re, geom[i].d1 / geom[i].d2);

				
				deltaP = ksi * geom[i].dx * G1[i] * G1[i] / (2 * (properties.density(P[i], h[i]) * geom[i].s * geom[i].s * (geom[i].d2 - geom[i].d1)));

				P[i] = P[i-1] - abs(deltaP);
			}
			
			dG = sqrt((2 * abs(pvuh - P[P.size() - 1])*properties.density(P[0], h[0]) * geom[0].s * geom[0].s * (geom[0].d2 - geom[0].d1)) / (ksi*L));
			if (pvuh > P[P.size() - 1])
				G2[0] -= 0.001*dG ;
			else
				G2[0] += 0.001*dG;
			
		} while (abs(pvuh-P[P.size()-1])> 0.001);
		
	}
	void poteri() {
		double max_eps = 0;
		double Re;
		double ksi;
		double deltaP = 0;
		for (int i = (int)G1.size() - 2; i != 0; i--) {
			Re = 4 * abs(G1[i]) / (pi * (geom[i].d2 - geom[i].d1) * properties.viscosity(P[i], h[i]));
			if (geom[i].d2 > 0 && geom[i].d1 == 0) 
				ksi = pipe::round::xi(Re);			
			if (geom[i].d1 > 0 && geom[i].d2 > 0 && geom[i].d2 > geom[i].d1)
				ksi = pipe::ring::xi(Re, geom[i].d1 / geom[i].d2);
			deltaP = ksi * geom[i].dx * G1[i] * G1[i] / (2 * (properties.density(P[i], h[i]) * geom[i].s * geom[i].s * (geom[i].d2 - geom[i].d1)));
			P[i] = P[i + 1] + deltaP / 1.0e6;
		}
	}

	void potoki(int i, double a1, double a2, double a) {
		double lam = 2*a1 * a / (a + a1);
		if (melt[i - 1]) {
			//Tgk_ = melt[i - 1]->Tgv;
			qx_ =melt[i - 1]->q;
			//qx_ = lam* (T[i] - Tgk_) / (geom[i].dx / 2 + melt[i - 1]->dxm / 2);
		}
		else
			qx_ = lam * 2 * (T[i] - T[i - 1]) / (geom[i].dx + geom[i - 1].dx);

		lam = 2 * a2 * a / (a + a2);
		if (melt[i + 1]) {
			//Tgk = melt[i + 1]->Tgv_;
			qx = melt[i + 1]->q_;
			//qx = a2 * (Tgk - T[i]) / (geom[i].dx / 2 + melt[i + 1]->dxm / 2);
		}
		else
			qx = 2 * a2 * (T[i + 1] - T[i]) / (geom[i].dx + geom[i + 1].dx);
	}

	void BoundOpen(vector<t_val> &Bound, string path) {
		ifstream file(path);
		if (!file)
			Error::readFailed(path + "(can't open file)");
		double t;
		double val;
		while (file >> t && file >> val)
			Bound.push_back(t_val(t, val));
		if (!Bound.size())
			Error::readFailed(path + "(reading error)");
	}

	void BoundOpenPG(vector<t_val>& Bound1, vector<t_val>& Bound2, string path) {
		ifstream file(path);
		if (!file)
			Error::readFailed(path + "(can't open file)");
		double t;
		double val1, val2;
		while (file >> t && file >> val1 && file >> val2) {
			if (abs(val1) >= 0)
				Bound1.push_back(t_val(t, val1));
			if (val2)
				Bound2.push_back(t_val(t, val2));
		}
		if (!Bound1.size() && !Bound2.size())
			Error::readFailed(path + "(reading errorPG)");
	}
	void BoundOpen(string PathH1, string PathH2, string PathG, string PathP) {
		BoundOpen(BoundH1, PathH1);
		BoundOpen(BoundH2, PathH2);
		BoundOpenPG(BoundG, BoundPin, PathG);
		BoundOpen(BoundP, PathP);
	}
	bool MassConservation() {
		for (int i = 1; i < (int)G1.size() - 1; i++) {
			G1[i] = -G2[i - 1];
            if (it_stat)
                G2[i] = -G1[i];
            else
                G2[i] = (properties.density(P_[i], h_[i]) - properties.density(P[i], h[i]))*geom[i].dx *geom[i].s / Time::step - G1[i];
		}
		return false;
	}
	bool EnergyConservation() {
		double max_eps = 0.0;
		for (int i = 1; i < (int)G1.size() - 1; i++) {
			double h_prev = h[i];
			unsigned c1 = 1;
			unsigned c2 = 1;
			if (G1[i] > 0)
				c1 = 0;
			if (G2[i] > 0)
				c2 = 0;
			double th1 = properties.thermalConductivity(P[i - 1], h[i - 1]);
			double th = properties.thermalConductivity(P[i], h[i]);
			double th2 = properties.thermalConductivity(P[i + 1], h[i + 1]);
			double dx1 = 0.5*(geom[i - 1].dx + geom[i].dx);
			double dx2 = 0.5*(geom[i + 1].dx + geom[i].dx);
			double s1 = 0.5*(geom[i - 1].s + geom[i].s);
			double s2 = 0.5*(geom[i + 1].s + geom[i].s);
			double l1 = 2 * th1*th / (th1 + th)*s1 / dx1;
			double l2 = 2 * th2*th / (th2 + th)*s2 / dx2;
			T[i] = properties.temperature(P[i], h[i]);
            if (it_stat)
                h[i] = 0.5*h_[i] + 0.5*(Q[i] - G1[i] * h[i - 1] * c1 - G2[i] * h[i + 1] * c2 - l1 * (T[i] - T[i - 1]) - l2 * (T[i] - T[i + 1])) /
                               (G1[i] * (1 - c1) + G2[i] * (1 - c2));
            else
                h[i] = (Q[i] + h_[i] * geom[i].s*geom[i].dx*properties.density(P_[i], h_[i]) / Time::step - G1[i] * h[i - 1] * c1 - G2[i] * h[i + 1] * c2 - l1 * (T[i] - T[i - 1]) - l2 * (T[i] - T[i + 1])) /
				(geom[i].s*geom[i].dx*properties.density(P[i], h[i]) / Time::step + G1[i] * (1 - c1) + G2[i] * (1 - c2));

			max_eps = max(max_eps, abs((h_prev - h[i]) / h[0]));
		}
		return (max_eps > epsilon);
	}
	bool EnergyConservationV2(int count) {
		double max_eps = 0.0;
		for (int i = 1; i < (int)G1.size() - 1; i++) {
			double h_prev = h[i];
			unsigned c1 = 1;
			unsigned c2 = 1;

			if (G1[i] > 0)
				c1 = 0;
			if (G2[i] > 0)
				c2 = 0;

			double r = properties.density(P[i], h[i]);
			double r_ = properties.density(P_[i], h_[i]);		
			double th1 = properties.thermalConductivity(P[i - 1], h[i - 1]);

			if (i == 1)
				th1 = 0;

			double th = properties.thermalConductivity(P[i], h[i]);
			double th2 = properties.thermalConductivity(P[i + 1], h[i + 1]);

			if (i == T.size() - 2)
				th2 = 0;
			
			double s1 = 0.5*(geom[i - 1].s + geom[i].s);
			double s2 = 0.5*(geom[i + 1].s + geom[i].s);
			
			T[i] = properties.temperature(P[i], h[i]);
			

			potoki(i, th1, th2, th);		
			
			
			if (melt[i] && geom[i].d1) {
				
			}
			else
				h[i] = (Q[i] + h_[i] * geom[i].s*geom[i].dx*properties.density(P_[i], h_[i]) / Time::step - G1[i] * h[i - 1] * c1 - G2[i] * h[i + 1] * c2 - qx_*s1 + qx*s2) /
				(geom[i].s*geom[i].dx*properties.density(P[i], h[i]) / Time::step + G1[i] * (1 - c1) + G2[i] * (1 - c2));


			max_eps = max(max_eps, abs(h_prev - h[i]));
		}
		return (max_eps > epsilon);

	}

	bool BoundaryConditions(vector<t_val> &Bound, double &val) {
		for (int i = 0; i < Bound.size() - 1; i++)
			if (Time::actual >= Bound[i].time && Time::actual <= Bound[i + 1].time) {
				val = Bound[i].value + (Time::actual - Bound[i].time) / (Bound[i + 1].time - Bound[i].time)*(Bound[i + 1].value - Bound[i].value);
				if (i > 0)
					Bound.erase(Bound.begin());
				return true;
				break;
			}
		return false;
	}
	void BoundaryConditions() {
		double val = 0.0;

		if (BoundG.size())
			if (BoundaryConditions(BoundG, val))
				G2[0] = val;
		if (BoundPin.size())
			if (BoundaryConditions(BoundPin, val)) {
				P[0] = val;
				rashod();
			}
		if (BoundaryConditions(BoundH1, val)) {
			h[0] = val;
			T[0] = properties.temperature(P[0], h[0]);
		}
		if (BoundaryConditions(BoundH2, val)) {
			h[h.size() - 1] = val;
			T[h.size() - 1] = properties.temperature(P[h.size() - 1], h[h.size() - 1]);
		}
		if (BoundaryConditions(BoundP, val)) {
			P[P.size() - 1] = val;
			T[h.size() - 1] = properties.temperature(P[h.size() - 1], h[h.size() - 1]);
		}
	}
public:
	Channel(channel_info info) {
		melt.resize(info.N);
		T.resize(info.N);
		P.resize(info.N);
		h.resize(info.N);
		G1.resize(info.N);
		G2.resize(info.N);
		G0 = &G2[0];
		P_.resize(info.N);
		h_.resize(info.N);
		geom.resize(info.N);
		Q.resize(info.N);
		alpha.resize(info.N);

		properties.init(info.properties);

		IsItMetal = (properties.Pr(0.0, 0.0) < 0.01);

		BoundOpen(info.BoundH1, info.BoundH2, info.BoundG, info.BoundP);

		for (int i = 0; i < info.N; i++) {
			Q[i] = 0.0;			
			h[i] = (BoundH1[0].value + BoundH2[0].value) / 2;
			h_[i] = h[i];
		}

		h[0] = BoundH1[0].value;
		h[h.size() - 1] = BoundH2[0].value;
		h_[0] = h[0];
		h_[h.size() - 1] = h[h.size() - 1];
		T[0] = properties.temperature(P[0], h[0]);
		T[h.size() - 1] = properties.temperature(P[h.size() - 1], h[h.size() - 1]);
		//Geometry
		shag = info.shag;
		L = info.L;
		double dx = L / (info.N - 2);
		for (auto& g : geom) {//todo
			g.s = info.S;
			g.dx = dx;
			g.d1 = info.d1;
			g.d2 = info.d2;
		}


		for (int i = 0; i < (info.nomera.size()) / 2; i++) {
			for (int j = info.nomera[i * 2]; j < info.nomera[2 * i + 1] + 1; j++) {
				geom[j].d1 = info.diametr[i];
				geom[j].d2 = info.Diametr[i];
				geom[j].dx = info.l[i] / (info.nomera[2*i+1] - info.nomera[2 * i] + 1);
			}
		}
		for (int i = 0; i < geom.size(); i++) {
			geom[i].s = pi * geom[i].d2*geom[i].d2 / 4 - pi * geom[i].d1*geom[i].d1 / 4;
			geom[i].phi = info.phi;
			geom[i].Dvn = info.Dvn;
		}


		geom[0].dx = 0.0;
		geom[geom.size() - 1].dx = 0.0;
		if (BoundG.size()) {
			for (int i = 0; i < info.N; i++) {
				G2[i] = BoundG[0].value;
				G1[i] = -G2[i];
				P[i] = BoundP[0].value;
				P_[i] = P[i];
				T[i] = properties.temperature(P[i], h[i]);
			}
		}
		
		if (BoundPin.size()) {
			P[0] = BoundPin[0].value;
			for (int i = 0; i < info.N; i++) {
				if (i)
					P[i] = BoundP[0].value;

				P_[i] = P[i];
				T[i] = properties.temperature(P[i], h[i]);
				}
			rashod();
			for (int i = 0; i < info.N; i++) {
				G2[i] = G2[0];
				G1[i] = -G2[i];
			}
		}
	}
	void calc_static() {
		unsigned count = 0;
		while ((MassConservation() || EnergyConservationV2(count) || count < 5) && count < difence)
			count++;
		if (geom[1].d1 == 0)
			poteri();
		for (int i = 0; i < h.size(); i++) {
			h_[i] = h[i];
			P_[i] = P[i];
		}
	}

	void calc() {
		BoundaryConditions();
		unsigned count = 0;
		while ((MassConservation() || EnergyConservationV2(count) || count < 5) && count < difence)
			count++;
		if (geom[1].d1 == 0)
			poteri();
		for (int i = 0; i < h.size(); i++) {
			h_[i] = h[i];
			P_[i] = P[i];
		}
	}
	void Stefan() {
		if (!IsItMetal)
			return;
		int nmelt = 0;
		for (int i = 1; i < (T.size() - 1); i++)
			if (melt[i])
				nmelt++;
		for (int j = 0; j < (nmelt + 1); j++)
		for (int i = 1; i < (T.size() - 1); i++) {

			double Tphase = properties.temperature(P[i], properties.h_l(P[i]));
			double r = properties.density(P[i], h[i]);
			double s1 = 0.5*(geom[i - 1].s + geom[i].s);

			if((T[i - 1] <= Tphase || T[i + 1] <= Tphase) && h[i] >= properties.solid(P[i]) && h[i] <= properties.liquid(P[i]) && !melt[i])
				melt[i] = new dvumer(h[i], geom[i].dx,&h[i - 1], &h[i + 1], &melt[i - 1], &melt[i + 1], P[i], properties, geom[i].d2 / 2, geom[i].d1, (G2[i - 1] / (r * s1)), shag, geom[i].phi, T.size());//nachalinue usl
		
			if (melt[i]){
				h[i] = melt[i]->raschet( i, *(geom[i].Twall));
			}

			if ((melt[i] && melt[i]->set) && ((!melt[i - 1] && !melt[i + 1]) ||
				(melt[i - 1] && melt[i + 1] && melt[i - 1]->set && melt[i + 1]->set) ||
				(melt[i - 1] && melt[i - 1]->set && !melt[i + 1]) ||
				(melt[i + 1] && melt[i + 1]->set && !melt[i - 1])
				))
			{
				delete melt[i];
				melt[i] = nullptr;
				if (melt[i])
					exit(1);
				hlop << "zacrut " << i << "vremya " << Time::actual << endl;
			}
		}
		for (int i = 1; i < (T.size() - 1); i++) {
			if (melt[i]) {
				melt[i]->pred();
			}
		}
	}
};
