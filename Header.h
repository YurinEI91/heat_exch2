#pragma once
//STL
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>
#include <chrono>

//#include <thread>
//#include <random>

//libs
#include <math.h>

//const
double const pi = 3.14159;
double const g = 9.82;
double const epsilon = 1.0e-2; //error
unsigned const difence = 100;
unsigned channel_count; //channel count
const bool parallel = false;
std::vector < double > Par;
bool it_stat = true;

//vars
namespace Time {
	unsigned stat = 0;
	double actual = 0.0;
	double step = -0.01;
	double duration = -1.0;
	int output = -1;
};

//struct
struct channel_info {
	std::string properties = "";
	std::string BoundH1 = "";
	std::string BoundH2 = "";
	std::string BoundG = "";
	std::string BoundP = "";
	int N = 0;
	double L = NAN;
	double S = NAN;
	double D = NAN;
	double Dvn = NAN;
	double d1 = NAN;
	double d2 = NAN;
	std::vector <int> nomera;
	std::vector <double> diametr;
	std::vector <double> Diametr;
	std::vector <double> l;
	double phi;
	double shag=NAN;
	int kd = 0;
};
enum wallGeometry { cylindrical,flat };
enum pipeGeometry { roundPipe,ringPipe,puchok};
//func
using namespace std;
namespace Error {
	void message(string message) {
		cout << message;
		cin.ignore();
		exit(1);
	}
	void readFailed(string path) {
		message("Error:\n\tRead failed: " + path);
	}
};

//local
#include "Properties.h"

#include "Alpha.h"
#include "Stefan.h"
#include "Chanel.h"
#include "Thermal.h"
//#include "Picture.h"

//vars
std::unique_ptr<PropertiesW> wall;
std::vector<Channel> channel;
std::vector<HeatStructure> heatStructure;
#include "Input.h"
#include "Output.h"

class Timer {
private:
	chrono::time_point<chrono::high_resolution_clock> t1;
public:
	bool pause;
	Timer() :pause(true) {
		using namespace chrono;
		t1 = high_resolution_clock::now();
	}
	~Timer() {
		using namespace chrono;
		auto t2 = high_resolution_clock::now();
		cout << "press\n";
		cout << endl << "elapsed_time:\t" << duration_cast<chrono::milliseconds>(t2 - t1).count() << " ms" << endl;
		if (pause)
			cin.ignore();
	}
}timer;
