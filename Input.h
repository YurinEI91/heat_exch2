using namespace std;
namespace input {
	ifstream file;
	string word;
	string path = "inputbrest.html";

	template <typename T>
	void readPair(string key, T &value) {
		if (word == key && !(file >> value))
			Error::readFailed(path + " :" + key);
	}
	void findP_MainInfo() {
		while (file >> word && word != "<p>");
		while (file >> word && word != "</p>") {
			readPair("step", Time::step);
			readPair("static", Time::stat);
			readPair("duration", Time::duration);
			readPair("output", Time::output);
			readPair("channel", channel_count);
		}
		if (Time::step < 0 || Time::duration < 0 || Time::output < 0)
			Error::readFailed(path + " : time (step / duration / output)");
		if (channel_count < 1)
			Error::readFailed(path + " : channel (count)");
	}
	channel_info findP_Channel() {

		double reader;
		channel_info Inf;
		word = "";
		while (file >> word && word != "<p>");
		while (file >> word && word != "</p>") {
			if (word[0] == '<')
				while (word.size()) {
					word.erase(0, 1);
					if (word[0] == '>') {
						word.erase(0, 1);
						break;
					}
				}
			readPair("properties", Inf.properties);
			readPair("H1", Inf.BoundH1);
			readPair("H2", Inf.BoundH2);
			readPair("G", Inf.BoundG);
			readPair("P", Inf.BoundP);
			readPair("N", Inf.N);
			readPair("L", Inf.L);
			readPair("D", Inf.D);
			readPair("d1", Inf.d1);
			readPair("d2", Inf.d2);
			readPair("Dvn", Inf.Dvn);
			readPair("S", Inf.S);
			readPair("Phi", Inf.phi);
			readPair("shag", Inf.shag);
			readPair("diffR", k);
			if (k > 1) {
				for (int i = 0; i < k; i++) {
					file >> reader;
					Inf.nomera.push_back(reader);
					file >> reader;
					Inf.nomera.push_back(reader);
					file >> reader;
					Inf.diametr.push_back(reader);
					file >> reader;
					Inf.Diametr.push_back(reader);
					file >> reader;
					Inf.l.push_back(reader);
				}
				k = 0;
			}
		}
		Inf.nomera.shrink_to_fit();
		Inf.diametr.shrink_to_fit();
		Inf.Diametr.shrink_to_fit();
		Inf.l.shrink_to_fit();
		if (Inf.S != Inf.S) {
			if (Inf.d1 != Inf.d1)
				Inf.S = pi * Inf.d2*Inf.d2 / 4;
			else
				Inf.S = pi * (Inf.d2*Inf.d2 / 4 - Inf.d1*Inf.d1 / 4);
		}
		if (Inf.D != Inf.D) {
			if (Inf.d1 != Inf.d1)
				Inf.D = 4.0*Inf.S / pi / Inf.d2;
			else
				Inf.D = 4.0*Inf.S / pi / (Inf.d2 + Inf.d1);
		}
		if (Inf.S != Inf.S || Inf.N != Inf.N || Inf.L != Inf.L || Inf.D != Inf.D)
			Error::readFailed(path + " : (S/N/L)");
		return Inf;
	}
	void findP_HeatStructure() {
		unsigned n = 2;
		string path_wall;
		int A_i = -1;
		int B_i = -1;
		unsigned A_begin = 0;
		unsigned B_begin = 0;
		unsigned A_end = 0;
		unsigned B_end = 0;
		word = "";
		while (file >> word && word != "<p>");
		while (file >> word && word != "</p>") {
			readPair("properties", path_wall);
			readPair("cellCount", n);
			for (int i = 0; i < channel.size(); i++) {
				if (A_i < 0) {
					readPair(channel[i].name, A_begin);
					if (A_begin > 0) {
						if (!(file >> A_end))
							Error::readFailed(path + " heat structure " + channel[i].name);
						A_i = i;
						break;
					}
				}
				if (B_i < 0) {
					readPair(channel[i].name, B_begin);
					if (B_begin > 0) {
						if (!(file >> B_end))
							Error::readFailed(path + " heat structure " + channel[i].name);
						B_i = i;
						break;
					}
				}
			}
		}
		if (n < 2)
			Error::readFailed(path + " heat structure cellCount");
		if (A_i < 0 || B_i < 0)
			return;
		if (!wall)
			wall.reset(new PropertiesW(path_wall));
		else
			wall->add(path_wall);
        
        int direction = 1;
        if (B_begin>B_end)
            direction=-1;
        for (; A_begin <= A_end; A_begin++, B_begin += direction)
            heatStructure.push_back(HeatStructure(wall->last(), channel[A_i], A_begin, channel[B_i], B_begin, n));
        
		if (word == "</p>")
			findP_HeatStructure();
		heatStructure.shrink_to_fit();
	}
	int readFile() {
		//open file
		file.open(path);
		if (!file)
			Error::readFailed(path);

		//read main info
		findP_MainInfo();

		//read channels
		string name = "I";
		for (int i = 0; i < channel_count; i++) {
			channel.push_back(Channel(findP_Channel()));
			channel[i].name = name;
			name += "I";
		}
		channel.shrink_to_fit();

		//read heat structures
		findP_HeatStructure();

		file.close();

		return 0;
	}
	int initialization = readFile();
};
