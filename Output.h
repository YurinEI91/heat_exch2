using namespace std;
ofstream moshi("moshi.txt");
namespace output {
	ofstream file_special("crit.txt");
	template <typename T>
	string to_string2(T x) {
		stringstream str;
		str.precision(8);
		str << x;
		return str.str();
	}
	template <typename T>
	string to_string_smart(T x) {
		if (x > 1.0e-2 && x < 1.0e3)
			return to_string2(x);
		stringstream str;
		str << scientific;
		str.precision(8);
		str << x;
		return str.str();
	}
	const int N = int((Time::output + Time::step / 2) / Time::step);
	int n = 0;
	int i = N;
	class html {
	private:
		ofstream file;
		template <typename T>
		void line(T x) {
			file << "<td>" << to_string_smart(x) << "</td>";
		}
	public:
		void add() {
			file << "\t\t\t<tr>\n";
			file << "\t\t\t";
			line(n*Time::output);
			for (auto &c : channel)
				for (auto &t : c.T)
					line(t);
			for (auto &c : channel)
				for (auto &g : c.G2)
					line(g);
			for (auto &c : channel)
				for (auto &p : c.P)
					line(p);
			for (auto &c : channel)
				for (auto &h : c.h)
					line(h);
			file << "\t\t\t</tr>\n";
		}
		html() {
			int n = 0;
			for (auto &c : channel) {
				n += c.T.size();
			}

			file.open("output.html");
			file << "<!DOCTYPE html>\n";
			file << "<html>\n";
			file << "\t<body>\n";
			file << "\t\t<table border=\"1\" cellspacing=\"0\">\n";
			file << "\t\t\t<tr>\n";
			file << "\t\t\t\t<th rowspan=\"3\">Time, s</th>\n";
			file << "\t\t\t\t<th colspan=\"" << n << "\">Temperature, K</th>\n";
			file << "\t\t\t\t<th colspan=\"" << n << "\">Mass Flow, kg/s</th>\n";
			file << "\t\t\t\t<th colspan=\"" << n << "\">Pressure, MPa</th>\n";
			file << "\t\t\t\t<th colspan=\"" << n << "\">Enthalpy, kJ/kg</th>\n";
			file << "\t\t\t</tr>\n";

			string chan = "I";
			string str = "";

			for (auto &c : channel) {
				str += "\t\t\t\t<th colspan=\"" + to_string(c.T.size()) + "\">" + chan + "</th>\n";
				chan += "I";
			}

			file << "\t\t\t<tr>\n";
			file << str;
			file << str;
			file << str;
			file << str;
			file << "\t\t\t</tr>\n";

			str = "\t\t\t\t";
			for (auto &c : channel) {
				for (int i = 0; i < c.T.size(); i++)
					str += "<th  style=\"min-width:80px\">" + to_string(i) + "</th> ";
			}
			str += "\n";

			file << "\t\t\t<tr>\n";
			file << str;
			file << str;
			file << str;
			file << str;
			file << "\t\t\t</tr>\n";
			file << scientific;
			file.precision(3);
		}
		~html() {
			file << "\t\t</table>\n";
			file << "\t</body>\n";
			file << "</html>";
			file.close();
		}
	};
	html HTML;
	void special() {
		for (int i = 0; i < channel[0].T.size(); ++i) {
			file_special << i << channel[0].h[i] << std::endl;
		}
	}
	unsigned count_s = 0;
	void special2() {
		ofstream file(to_string(count_s * Time::output) + ".txt");

		unsigned i_l = 0;
		unsigned i_pb_l = 0;
		/*unsigned i_v = 70;
		unsigned i_pb_v = 1;
		for (unsigned i = 0; i < 30; i++) {
			for (unsigned d = 0; d < 3; d++) {
				file << channel[0].T[i_v];
				if (channel[2].melt[i_pb_v]) {
					for (unsigned k = 0; k < 5; k++)
						file << '\t' << channel[2].melt[i_pb_v]->T[3 - d][k];
				}
				else {
					for (unsigned k = 0; k < 5; k++)
						file << '\t' << channel[2].T[i_pb_v];
				}
				file << std::endl;
			}
			i_pb_v++;
			i_v--;
		}*/


		file.close();
		count_s++;
	}
	void regular() {
		if (i == N) {
			special();
			i = 0;
			HTML.add();
			n++;
			special2();
			moshi << abs((channel[0].h[0] - channel[0].h[1])*channel[0].G2[0]) << endl;
		}
		i++;
	}
}