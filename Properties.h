#pragma once
using namespace std;
class Properties {
public:
	bool metal; // is it liquid metal? //todo
	bool saturation; //saturation indicator
private:
	struct subTable {
		double P;
		int begin;
		int end;
		subTable(double P_, int begin_, int end_) {
			end = end_;
			begin = begin_;
			P = P_;
		}
	};
	vector <subTable> P_index;
	vector <int> T_index;
	vector <int> mu_index;
	vector<double> T;
	vector<double> p;
	vector<double> rho;
	vector<double> Cp;
	vector<double> mu;
	vector<double> lam;
	vector<double> h;
	double interpolation(double H, vector<double>& Y, int up, int bottom) {
		saturation = false;

		if (H <= h[up])
			return Y[up];
		if (H >= h[bottom])
			return Y[bottom];

		int left = up;
		int right = bottom;
		while (right - left - 1) {
			int center = (right + left) / 2;
			if (H <= h[center])
				right = center;
			else
				left = center;
		}

		if (T[left] == T[right]) {

			saturation = true;
			double x = (H - h[left]) / (h[right] - h[left]);
			double beta = rho[left] * x / (rho[right] + x * (rho[left] - rho[right]));
			return Y[left] + (Y[right] - Y[left]) * beta;
		}
		return Y[left] + (Y[right] - Y[left]) * (H - h[left]) / (h[right] - h[left]);
	}
	double interpolation(double P, double H, vector<double> & Y) {

		int left = 0;
		if (P <= P_index[left].P)
			return interpolation(H, Y, P_index[left].begin, P_index[left].end);

		int right = (int)P_index.size() - 1;
		if (P >= P_index[right].P || left == right)
			return interpolation(H, Y, P_index[right].begin, P_index[right].end);

		while (right - left - 1) {
			int center = (right + left) / 2;
			if (P < P_index[center].P)
				right = center;
			else
				left = center;
		}

		if (left == right)
			return interpolation(H, Y, P_index[left].begin, P_index[left].end);

		double Y_left = interpolation(H, Y, P_index[left].begin, P_index[left].end);
		bool lok_sat = saturation;
		double Y_right = interpolation(H, Y, P_index[right].begin, P_index[right].end);
		if (lok_sat) {
			saturation = true;
			//todo
		}
		return Y_left + (Y_right - Y_left) * (P - P_index[left].P) / (P_index[right].P - P_index[left].P);
	}
	vector<double> interpolation(vector<double> & Y, int up) {
		int n = T_index[up];
		return vector<double>{ Y[n], Y[n + 1] };
	}

	vector<double> solidliquid(vector<double> & Y, int up) {
		int n = mu_index[up];
		return vector<double>{ Y[n], Y[n + 1] };
	}

	vector<double> interpolation(double P, vector<double> & Y) {
		int left = 0;
		if (P <= P_index[left].P)
			return interpolation(Y, left);
		int right = (int)P_index.size() - 1;
		if (P >= P_index[right].P || left == right)
			return interpolation(Y, right);

		while (right - left - 1) {
			int center = (right + left) / 2;
			if (P < P_index[center].P)
				right = center;
			else
				left = center;
		}

		if (left == right)
			return interpolation(Y, left);

		vector<double> Y_left = interpolation(Y, left);
		vector<double> Y_right = interpolation(Y, right);
		for (int i = 0; i < 2; ++i)
			Y_left[i] += (Y_right[i] - Y_left[i]) * (P - P_index[left].P) / (P_index[right].P - P_index[left].P);
		return vector <double> {Y_left};
	}

	vector<double> solidliquid(double P, vector<double> & Y) {
		int left = 0;
		if (P <= P_index[left].P)
			return solidliquid(Y, left);
		int right = (int)P_index.size() - 1;
		if (P >= P_index[right].P || left == right)
			return solidliquid(Y, right);

		while (right - left - 1) {
			int center = (right + left) / 2;
			if (P < P_index[center].P)
				right = center;
			else
				left = center;
		}

		if (left == right)
			return solidliquid(Y, left);

		vector<double> Y_left = solidliquid(Y, left);
		vector<double> Y_right = solidliquid(Y, right);
		for (int i = 0; i < 2; ++i)
			Y_left[i] += (Y_right[i] - Y_left[i]) * (P - P_index[left].P) / (P_index[right].P - P_index[left].P);
		return vector <double> {Y_left};
	}

public:

	void init(string path) {
		ifstream file(path);

		double reader;
		while (file >> reader) {

			if (T.size())
				if (reader == T[T.size() - 1])
					T_index.push_back((int)(T.size() - 1));
			T.push_back(reader);

			if (file >> reader) {
				if (!p.size() || reader > p[p.size() - 1])
					P_index.push_back(subTable(reader, (int)p.size(), 0));
				p.push_back(reader);
			}
			else
				Error::readFailed(path);

			if (file >> reader)
				h.push_back(reader);
			else
				Error::readFailed(path);

			if (file >> reader)
				rho.push_back(reader);
			else
				Error::readFailed(path);

			if (file >> reader)
				Cp.push_back(reader);
			else
				Error::readFailed(path);

			if (file >> reader) {
				if (mu.size())
					if (reader > 0 && mu[mu.size() - 1] < 0)
						mu_index.push_back((int)(mu.size() - 1));
				mu.push_back(reader * 1.0e-6);
			}
			else
				Error::readFailed(path);

			if (file >> reader)
				lam.push_back(reader);
			else
				Error::readFailed(path);
		}
		if (T.size() < 1)
			Error::readFailed(path);

		T.shrink_to_fit();
		p.shrink_to_fit();
		h.shrink_to_fit();
		rho.shrink_to_fit();
		Cp.shrink_to_fit();
		mu.shrink_to_fit();
		lam.shrink_to_fit();
		P_index.shrink_to_fit();
		P_index[P_index.size() - 1].end = (int)T.size() - 1;
		for (int i = 0; i < P_index.size() - 1; i++)
			P_index[i].end = P_index[i + 1].begin - 1;
		file.close();
	}


	double density(double P, double H) {
		return interpolation(P, H, rho);
	}
	double viscosity(double P, double H) {
		return interpolation(P, H, mu);
	}
	double thermalConductivity(double P, double H) {
		return interpolation(P, H, lam);
	}
	double heatCapacity(double P, double H) {
		return interpolation(P, H, Cp);
	}
	double temperature(double P, double H) {
		return interpolation(P, H, T);
	}
	double entalpy(double P, double T) {
		return interpolation(P, T, h);
	}
	double Pr(double P, double H) {
		return viscosity(P, H)* heatCapacity(P, H) / thermalConductivity(P, H);
	}


	double x(double P, double H) {
		vector<double> h_l_v = interpolation(P, h);
		return (H - h_l_v[0]) / (h_l_v[1] - h_l_v[0]);
	}
	bool is_it_saturation(double P, double H) {
		if (P > 22.1)
			return false;
		vector<double> h_l_v = interpolation(P, h);
		if (H >= h_l_v[0] && H <= h_l_v[1])
			return true;
		return false;
	}
	double h_l(double P) {
		vector<double> h_l_v = interpolation(P, h);
		return h_l_v[0];
	}
	double h_v(double P) {
		vector<double> h_l_v = interpolation(P, h);
		return h_l_v[1];
	}
	double h_l_v(double P) {
		vector<double> h_l_v_ = interpolation(P, h);
		return h_l_v_[1] - h_l_v_[0];
	}
	double rho_l(double P) {
		vector<double> rho_l_v = interpolation(P, rho);
		return rho_l_v[0];
	}
	double rho_v(double P) {
		vector<double> rho_l_v = interpolation(P, rho);
		return rho_l_v[1];
	}
	double liquid(double P) {
		vector<double> h_liq_sol = solidliquid(P, h);
		return h_liq_sol[1];
	}
	double solid(double P) {
		vector<double> h_liq_sol = solidliquid(P, h);
		return h_liq_sol[0];
	}

	double viscos_liq(double P) {
		vector<double> viscos_liq = interpolation(P, mu);
		return viscos_liq[0];
	}
	double lam_liq(double P) {
		vector<double> lam_liq = interpolation(P, lam);
		return lam_liq[0];
	}
	double Cp_liq(double P) {
		vector<double> Cp_liq = interpolation(P, Cp);
		return Cp_liq[0];
	}
	double Pr_liq(double P) {
		double Pr_liq = viscos_liq(P) * Cp_liq(P) / lam_liq(P);
		return Pr_liq;
	}

	double viscos_vap(double P) {
		vector<double> viscos_vap = interpolation(P, mu);
		return viscos_vap[1];
	}
	double lam_vap(double P) {
		vector<double> lam_vap = interpolation(P, lam);
		return lam_vap[1];
	}
	double Cp_vap(double P) {
		vector<double> Cp_vap = interpolation(P, Cp);
		return Cp_vap[1];
	}
	double Pr_vap(double P) {
		double Pr_vap = viscos_vap(P) * Cp_vap(P) / lam_vap(P);
		return Pr_vap;
	}
};
class PropertiesW {
private:
	PropertiesW* next;
	double interpolation(double X, vector<double>& Y) {
		if (X != X)
			return NAN;
		if (X <= T[0])
			return Y[0];
		if (X >= T[T.size() - 1])
			return Y[T.size() - 1];

		int left = 0;
		int right = (int)T.size() - 1;
		while (right - left - 1) {
			int center = (right + left) / 2;
			if (X < T[center])
				right = center;
			else
				left = center;
		}
		return Y[left] + (Y[right] - Y[left]) * (X - T[left]) / (T[right] - T[left]);
	}
	vector<double> T;
	vector<double> rho;
	vector<double> Cp;
	vector<double> lam;
public:
	PropertiesW(string path) :next(nullptr) {
		ifstream file(path);
		double reader;
		while (file >> reader) {
			T.push_back(reader);

			if (file >> reader)
				rho.push_back(reader);
			else
				Error::readFailed(path);

			if (file >> reader)
				Cp.push_back(reader);
			else
				Error::readFailed(path);

			if (file >> reader)
				lam.push_back(reader);
			else
				Error::readFailed(path);
		}
		if (T.size() < 1)
			Error::readFailed(path);
		T.shrink_to_fit();
		rho.shrink_to_fit();
		Cp.shrink_to_fit();
		lam.shrink_to_fit();
		file.close();
	}
	~PropertiesW() {
		if (next)
			delete next;
		next = nullptr;
	}
	double density(double T) {
		return interpolation(T, rho);
	}
	double heatCapacity(double T) {
		return interpolation(T, Cp);
	}
	double thermalConductivity(double T) {
		return interpolation(T, lam);
	}
	void add(string & path) {
		if (next)
			next->add(path);
		else
			next = new PropertiesW(path);
	}
	PropertiesW* last() {
		if (next)
			return next->last();
		return this;
	}
};