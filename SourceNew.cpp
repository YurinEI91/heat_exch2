
#include "Header.h"

void channel_step() {
	for (auto &c : channel) {
		c.calc();		
	}
}
void heatStructure_step() {
	for (auto &s : heatStructure)
		s.Q0();
	for (auto &s : heatStructure)
		s.calc();
}
void Stefan_step() {
	for (auto &c : channel) {
		c.Stefan();
	}
}
void channel_step_i(int i) {
	channel[i].calc();	
}
//void channel_step_parallel() {
//    thread thr[channel.size()];
//
//    for (int i = 0; i < channel.size(); i++)
//        thr[i] = thread(channel_step_i, i);
//
//    for (auto &t: thr)
//        t.join();
//}
void step() {// one time step *****************
	output::regular();
	//    if (parallel)
	//        channel_step_parallel();
	//    else
	channel_step();
	heatStructure_step();
	Stefan_step();
}

int main() {// main function ******************


//	//static **********************************
//	cout << "Static\n";
//	for (int i = 0; i < Time::stat / Time::step; i++) {
//		for (auto &c : channel)
//			c.calc_static();
//		heatStructure_step();
//	}
	
	//dynamic **********************************
	cout << "Dynamic\t(" << Time::duration << " s)\n";
	long long unsigned int n = 1;
	const long long unsigned int N = Time::duration / Time::step + 2;
	
	for (; n < N; n++) {
		Time::actual = n * Time::step;
        if(it_stat && Time::actual > Time::stat)
            it_stat = false;
		step();

	}
    for (int i = 0; i < channel.size(); i++)
        cout << channel[i].G2[0]*(channel[i].h[channel[i].h.size()-2]-channel[i].h[0]) << endl;
	ofstream output1("entalpy1.txt");
	ofstream output2("entalpy2.txt");
	ofstream output3("entalpy3.txt");
	ofstream par("par.txt");
	Par.shrink_to_fit();
	for (int i = 0; i < channel[0].T.size(); i++) {
		//if (i % 10 == 0)
			//cout << endl;
		output1 << channel[0].h[i] << endl;
	}
	for (int i = 0; i < channel[1].T.size(); i++)
		output2 << channel[1].h[i]<<endl;
	for (int i = 0; i < Par.size(); i++)
		par <<" " << Par[i] << endl;
    
    
	for (int i = 0; i < channel[2].T.size(); i++)
		output3 << channel[2].h[i] << endl;
	output2.close();
	output1.close();
	output3.close();
	Dvumer.close();	
	par.close();
	moshi.close();
	cout << "Program Completed\n";

	return 0;
}
