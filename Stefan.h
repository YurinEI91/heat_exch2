#pragma once
ofstream Dvumer("dvumer.txt");
using namespace std;
class dvumer {
public:
	double Qsumrad = 0;
	static const unsigned N = 5;
	static const unsigned K = 5;
	double T[N][K];
	double h[N][K];
	void pred() {
		for (int i = 0; i < N; i++)
			for (int j = 0; j < K; j++) {
				h_[i][j] = h[i][j];
			}
	}
private:
	static const unsigned C_STEP = 1;
	double G1sum = 0, rcssum = 0, delta = 0, w;
	double Twall;
	const double E = 0.5;
	double Tphase;
	double P;
	double shag, phi;
	double Qx, Qr, Qkonr2, Qkonr1;
	double lamdar, lamdar_;
	double lamdax_, lamdax;
	double h_[N][K]; double h__[N][K];	
	double G1[N][K]; double G2[N][K];
	double G3[N][K]; double G4[N][K];
	double G1__[N][K]; double G2__[N][K];
	double G3__[N][K]; double G4__[N][K];
	double Sproh[N]; double d[N];
	int c[N][K];//rashodu
	int z[N][K];//zamerz
	double R[K + 1];//koordinate graney KO
	double ro[K];//koordinate centrov KO
	double V[K]; double Sx[K]; double Sr[K + 1];
	double dtm, tom, tm, r, r_;
	//double tgv = 0;//temperature vnutr radiusa
	int ch;
	int Tk;
	double Qr1;
	double Qr2;
	double hsr = 0;
	double drstandard;
	double dr;
	double dr_;
	double Sum;
	double dx;
	int NMAX, nKO;
	bool perecrut[N];
	bool per = false;
	Properties *prop;//todo!!! you should get it in constructor prop = &prop_
	double hp; 
	void parametrs(int i, int j) {

		lamdax = 2 * prop->thermalConductivity(P, T[i][j])*prop->thermalConductivity(P, T[i + 1][j]) / (prop->thermalConductivity(P, T[i][j]) + prop->thermalConductivity(P, T[i + 1][j]));
		lamdax_ = 2 * prop->thermalConductivity(P, T[i][j])*prop->thermalConductivity(P, T[i - 1][j]) / (prop->thermalConductivity(P, T[i][j]) + prop->thermalConductivity(P, T[i - 1][j]));
		//lamdax = 0;//adyabatic prodol
		//lamdax_ = 0;//adyabatic prodol

		lamdar = 2 * prop->thermalConductivity(P, T[i][j])*prop->thermalConductivity(P, T[i][j + 1]) / (prop->thermalConductivity(P, T[i][j]) + prop->thermalConductivity(P, T[i][j + 1]));
		if (j == K - 1) //adyabatic
			lamdar = 0;
		
		if (nKO == 1)
			lamdax_ = 0;
		if (nKO == (NMAX - 2))
			lamdax = 0;		
		
		lamdar_ = 2 * prop->thermalConductivity(P, T[i][j - 1])*prop->thermalConductivity(P, T[i][j]) / (prop->thermalConductivity(P, T[i][j - 1]) + prop->thermalConductivity(P, T[i][j]));

		r = prop->density(P, h[i][j]);
		r_ = prop->density(P, h_[i][j]);

		
		if (i == 0)
			if (melt_)
				dxml_ = dxm;
			else
				dxml_ = dxm / 2 + dx/2;
		else
			dxml_ = dxm;

		if (i == N - 2)
			if (melt)
				dxml = dxm;
			else
				dxml = dxm / 2 + dx / 2;
		else
			dxml = dxm;

	}
	double Nusselt(int i, int j) {
		double Gsum = 0;
		for (int t = 0; t < K; t++)
			Gsum += abs(G2[i][t]);
		double Re = 4 * Gsum / (pi * d[i] * prop->viscosity(P, h[i][j + 1]));
		double Pe = Re * prop->Pr(P, h[i][j + 1]);
		if (Gsum == 0)
			return 1;
		phi *= pi / 180.0;
		if (!phi) //prodol'noe obtekanie
			return 7.55*shag - 20.0 / pow(shag, 13) + 0.041 / shag / shag * pow(Pe, 0.56 + 0.19*shag);
		return 2 * sqrt(Pe)*pow(sin(phi), 0.4);// Kirillov p. 123 (7.52)


	}
	void potoki(int i, int j, int c3, int c4) {
		// po osi x adyabatic
		Qx = Sx[j] * (lamdax*(T[i + 1][j] - T[i][j]) / dxml - lamdax_ * (T[i][j] - T[i - 1][j]) / dxml_);

		if (j == 0) {
			Qr2 = Sr[j] * (T[i][j] - Twall)*lamdar / (R[j] * log(ro[j] / R[j]));
			//qr2 = -qvnutr;
			Qkonr2 = 0;
		}
		else
		{
			Qr2 = Sr[j] * (T[i][j] - T[i][j - 1])*lamdar_ / (R[j] * log(ro[j] / ro[j - 1]));
			Qkonr2 = c3 * G3[i][j] * h[i][j - 1] * z[i][j];
		}
		if (j == 0 && channel_count == 1)
			Qr2 = 0;

		if (j == K - 1) {
			//qr1 = Sr[j+1]*(tg - T[i][j])*lamdar / (R[j + 1] * log(R[j + 1] / ro[j]));
			Qr1 = 0;
			Qkonr1 = 0;
		}
		else {
			Qr1 = Sr[j + 1] * (T[i][j + 1] - T[i][j])*lamdar / (R[j + 1] * log(ro[j + 1] / ro[j]));
			Qkonr1 = c4 * G4[i][j] * h[i][j + 1] * z[i][j];
		}

		if (h[i][j] < prop->solid(P) && h[i][j + 1] > prop->solid(P)) {
			double Nu = Nusselt(i, j);
			Qr2 *= Nusselt(i, j);
		}


		if (j == 0)
			Qsumrad += Qr2;

		Qr = Qr1 - Qr2;

	}
	void perecrutiesech() {

		for (int i = 1; i < N - 1; i++) {
			int k = 0;
			for (int j = 0; j < K; j++) {
				if (z[i][j] == 0)
					k++;
			}
			if (k == K) {
				Dvumer << "sechenie " << i << " perecruto";
				perecrut[i] = true;
				Sproh[i] = 0;
				for (int n = i; n < N - 1; n++)
					for (int j = 0; j < K; j++)
						G2[i][j] = G3[i][j] = G4[i][j] = G1[i][j] = c[i][j] = 0;
			}
		}
	}
	void zamerz(int i, int j) {
		if (h[i][j] <= prop->solid(P)) {
			G2[i - 1][j] = 0;
			G3[i][j + 1] = 0;
			G4[i][j - 1] = 0;
			G1[i][j] = 0;
			G2[i][j] = 0;
			G3[i][j] = 0;
			G4[i][j] = 0;
			c[i][j] = 0;
			c[i - 1][j] = 0;
			Sproh[i] -= Sx[j];
			d[i] += 2 * dr;
			z[i][j] = 0;
		}
	}
	void turbulent() {
		double hsredn;

		for (int i = 1; i < N - 1; i++) {
			Sum = 0;
			hsredn = 0;
			for (int j = 0; j < K; j++)
				Sum += prop->density(P, h[i][j])*V[j];
			for (int j = 0; j < K; j++)
				hsredn += prop->density(P, h[i][j])*V[j] * h[i][j] * z[i][j] / Sum;
			for (int j = 0; j < K; j++)
				h[i][j] = hsredn;
		}

	}
	void ottael(int i, int j) {
		if (h[i][j] > prop->liquid(P)) {
			z[i][j] = 1;
			c[i][j] = 1;
			c[i - 1][j] = 1;
		}
	}
	void stepmass() {

		for (size_t i = 1; i < N - 1; i++) {
			G1sum = 0;
			delta = 0;
			rcssum = 0;
			w = 0;
			for (size_t j = 0; j < K; j++) {
				r_ = prop->density(P, h_[i][j]);
				r = prop->density(P, h[i][j]);
				G1sum += G1[i][j];
				delta += (r - r_)*V[j] / dtm;
				rcssum += r * c[i][j] * Sx[j];
			}

			w = (-G1sum - delta) / (rcssum);
			if (!perecrut[i]) {
				for (size_t j = 0; j < K; j++)
				{
					r_ = prop->density(P, h_[i][j]);
					r = prop->density(P, h[i][j]);

					if (j > 0)
						G3[i][j] = -G4[i][j - 1];
					else
						G3[i][j] = 0;

					G2[i][j] = c[i][j] * w*r*Sx[j];

					G1[i + 1][j] = -G2[i][j];

					if (j == K - 1)
						G4[i][j] = 0;
					else
						G4[i][j] = -V[j] * (r - r_) / dtm - (G2[i][j] + G3[i][j] + G1[i][j]);

				}
			}
			else
				for (size_t j = 0; j < K; j++)
					G2[i][j] = G3[i][j] = G4[i][j] = G1[i][j] = 0;

			G1sum = 0;
			for (size_t j = 0; j < K; j++) {
				G1sum += G2[i][j];
			}
		}
	}

	void stepenergy() {
		Qsumrad = 0;
		for (int i = 1; i < N - 1; i++) {
			for (int j = 0; j < K; j++) {

				unsigned c1 = 1;
				unsigned c2 = 1;
				unsigned c3 = 1;
				unsigned c4 = 1;

				if (G1[i][j] >= 0)
					c1 = 0;
				if (G2[i][j] >= 0)
					c2 = 0;
				if (G3[i][j] >= 0)
					c3 = 0;
				if (G4[i][j] >= 0)
					c4 = 0;

				parametrs(i, j);
				potoki(i, j, c3, c4);


				h[i][j] = (prop->density(P, h_[i][j]) * h_[i][j] * V[j] / dtm + Qr + Qx - c1 * G1[i][j] * h[i - 1][j] - c2 * G2[i][j] * h[i + 1][j] - Qkonr2 - Qkonr1) /
					(V[j] * prop->density(P, h[i][j]) / dtm + G1[i][j] * (1 - c1) + G2[i][j] * (1 - c2) + G3[i][j] * (1 - c3) + G4[i][j] * (1 - c4));// with kinvekcia

				//h[i][j] = (dtm * (Qx + Qr) / (V[j])+ h_[i][j] * r_)/ r;//without konvekcia

				zamerz(i, j);
				ottael(i, j);

				T[i][j] = prop->temperature(P, h[i][j]);
			}
		}
		perecrutiesech();
	
	}
	
	void entalpymass() {
		hsr = 0;
		Sum = 0;
		for (int i = 1; i < N - 1; i++)
			for (int j = 0; j < K; j++)
				Sum += prop->density(P, h[i][j])*V[j];
		for (int i = 1; i < N - 1; i++)
			for (int j = 0; j < K; j++)
				hsr += prop->density(P, h[i][j])*V[j] * h[i][j] / Sum;
	}

	void zeidel(int iter) {
		for (int i = 0; i < N; i++)
			for (int j = 0; j < K; j++)
				if (iter) {
					h[i][j] = h__[i][j];
					h_[i][j] = h__[i][j];
					G2[i][j] = G2__[i][j];
					G1[i][j] = G1__[i][j];
					G3[i][j] = G3__[i][j];
					G4[i][j] = G4__[i][j];
				}
				else {
					h__[i][j] = h[i][j];
					G2__[i][j] = G2[i][j];
					G1__[i][j] = G1[i][j];
					G3__[i][j] = G3[i][j];
					G4__[i][j] = G4[i][j];
				}
	}
	void vuvod(int KO) {
		Dvumer << Time::actual << endl;
		Dvumer << KO << " phase procces\n";
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < K; j++) {
				Dvumer << h[i][j] << '\t';
			}
			Dvumer << endl;
		}
		Dvumer << endl;
	}
	void napor() {
		double Q = 0;
		double Q_ = 0;
		q = 0;
		q_ = 0;
		
		if (melt)
			for (int i = 0; i < K; i++) 			
				Q += Sx[i]*lamdax * (T[N-1][i] - T[N - 2][i]) / (dxm);			
		else
			for (int i = 0; i < K; i++)
				Q += Sx[i] * lamdax * (T[N - 1][i] - T[N - 2][i]) / (dxm/2 + dx/2);


		if (melt_)
			for (int i = 0; i < K; i++) 
				Q_ += Sx[i] * lamdax * (T[1][i] - T[i][0]) / ( dxm );
		else
			for (int i = 0; i < K; i++)
				Q_ += Sx[i] * lamdax * (T[1][i] - T[i][0]) / (dxm/2 + dx/2);

		/*Tgv = 0;
		Tgv_ = 0;
		for (int i = 0; i < K; i++) {
			Tgv_ += T[1][i];
			Tgv += T[N - 2][i];
		}
		for (int i = 0; i < N; i++) {
			Tradpotok += T[i][K - 1];
		}*/
		/*Tradpotok /= N;*/
		/*Tgv_ /= (K);
		Tgv /= (K);*/
		double S = 0;
		for (int i = 0; i < K; i++) 
			S+= Sx[i];
		
		q = Q/ (S);
		q_ = Q_ / (S);
	}
	void prov() {
		/*for (int i = 1; i < N - 1; i++)
			for (int j = 0; j < K; j++)
				if (T[i][j] < Tphase)
					Tk++;


		if (Tk == setk)
			set = true;
		else
			set = false;*/
		Tk = 0;
		
		for (int i = 1; i < N - 1; i++)
			for (int j = 0; j < K; j++)
				if (h[i][j] > hp)
					Tk++;


		if (Tk == setk)
			set = true;
		else
			set = false;
	}
public:
	int setk = (N - 2)*(K);
	double Tgv;
	double  dxm;
	double dxml, dxml_;
	double Tgv_;
	double q, q_;
	bool set = false;
	double Tradpotok;
	dvumer **melt, **melt_;
	double *hgk, *hgk_;

	dvumer(double hmacro, double x, double *hg_, double *hg, dvumer **Melt_, dvumer **Melt, double p, Properties &prop_, double Rtrub, double d1, double U, double Shag, double Phi, int Nn) {
		//qr = qrr;	
		NMAX = Nn;
		double R2 = Rtrub;
		double R1 = d1 / 2;
		double VV;
		VV = pi * (R2 * R2 - R1 * R1)*x;
		dxm = x / (N - 2)/cos(Phi*pi/180);
		R2 = sqrt(VV / pi/x * cos(Phi*pi/180) + R1 * R1);
		melt = Melt;
		melt_ = Melt_;

		hgk = hg;
		hgk_ = hg_;

		drstandard = (R2 - R1) / (K);
		dr = drstandard;
		P = p;
		Tphase = prop_.temperature(P, prop_.h_l(P));
		tom = Time::step;
		dtm = Time::step / C_STEP;
		dx = x;
		shag = Shag;
		phi = Phi;

		
		prop = &prop_;
		hp = prop->liquid(P);

		for (int i = 0; i < K + 1; i++)
			R[i] = dr * i + R1;//koordunate graney

		for (int i = 0; i < K; i++) {
			ro[i] = (R[i] + R[i + 1]) / 2;// koordinate centrev
		}

		//double deltaT = (Tg - Tg_) / (N-1);
		for (int i = 0; i < N; i++)
			for (int j = 0; j < K; j++) {
				h[i][j] = hmacro;
				T[i][j] = prop->temperature(P, h[i][j]);
				h_[i][j] = h[i][j];
				h__[i][j] = h[i][j];
				c[i][j] = 1;
				z[i][j] = 1;
			}
		for (int i = 1; i < N - 1; i++)
			for (int j = 0; j < K; j++) {
				zamerz(i, j);
				ottael(i, j);
			}

		/*for (int i = 0; i < K; i++) {
			T[0][i] = Tg_;
			h[0][i] = prop->entalpy(P, T[0][i]);
			h_[0][i] = h[0][i];
			h__[0][i] = h[0][i];
			T[N - 1][i] = Tg;
			h[N - 1][i] = prop->entalpy(P, T[N - 1][i]);
			h_[N - 1][i] = h[N - 1][i];
			h__[N - 1][i] = h[N - 1][i];
		}*/
		double sumS = 0;
		for (int i = 0; i < K; i++) {
			V[i] = pi * dxm * (pow(R[i + 1], 2) - pow(R[i], 2));//obiem KO
			Sx[i] = V[i] / dxm;
			sumS += Sx[i];
		}
		for (int i = 0; i < K + 1; i++) {
			Sr[i] = 2 * R[i] * pi*dxm;// ploshadi poverchosti
		}


		for (int j = 0; j < K; j++) {
			G2[0][j] = U * prop->density(P, h[0][j])*Sx[j];
			G1[1][j] = -G2[0][j];
			G2__[0][j] = G2[0][j];
			G1__[1][j] = G1[1][j];
		}
		double Gsum = 0;
		for (int j = 0; j < K; j++) {
			Gsum += G2[0][j];
		}

		for (int i = 0; i < N; i++) {
			Sproh[i] = sumS;
			d[i] = d1;
			for (int j = 0; j < K; j++) {
				G3[i][j] = 0;
				G4[i][j] = 0;
				G3__[i][j] = 0;
				G4__[i][j] = 0;
			}
		}
		/*for (int i = 0; i < N; i++) {
			T[i][K - 1] = Tr;
			h[i][K - 1] = prop->entalpy(P, Tr);
		}*/
	}

	~dvumer() {
		Dvumer << "razrushenie\n";
	}

	double raschet( int KO,  double Tw) {

		//zeidel(iter);
		hsr = 0;
		Tk = 0;
		set = false;
		nKO = KO;
		Twall = Tw;
		perecrutiesech();
		if (*melt) {
			for (int i = 0; i < K; i++) {
				T[N - 1][i] = (*melt)->T[0][i];
				h[N - 1][i] = (*melt)->h[0][i];
			}
		}
		else {
			for (int i = 0; i < K; i++) {
				h[N - 1][i] = *hgk;
				T[N - 1][i] = prop->temperature(P,*hgk);
			}
		}

		if (*melt_) {
			for (int i = 0; i < K; i++) {
				T[0][i] = (*melt_)->T[N - 1][i];
				h[0][i] = (*melt_)->h[N - 1][i];
			}
		}
		else
		{
			for (int i = 0; i < K; i++) {
				h[0][i] = *hgk_;
				T[0][i] = prop->temperature(P, *hgk_);
			}
		}

		for (int t = 0; t < C_STEP; t++) {
			stepmass();
			stepenergy();			
		}

		if ((Time::step / 2 - (int)(Time::actual / 2)) < Time::step / 2 && KO == 1 )
			//if (iter == 0)
				//if (Time::actual == 1000)
				vuvod(KO);//todo - call dvumer::vuvod in output.h

		entalpymass();

		prov(); //think about it!!

		napor();

		return hsr;
	}

};