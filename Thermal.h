#pragma once
class dvumer;
struct HeatStructure {
	struct Cell {
		double *T;
		double *Q;
		double *P;
		double *Qwall;
		double *h;
		double *G;
		double *G0;
		double *s;
		double tet;
		double *phi;		
		double S;
		double D;
		dvumer **melt;
		double Dvn;
		bool *metal;
		double *shag;
		Properties *prop;
		pipeGeometry PipeGeometry;
		channel_info info;
		double alpha(double Tw) {			
			double Nu = NAN;
			double Re;
			if (*G0 == 0) 
				Re = 0;	
			else 
				Re = abs(*G0) / *s * D / prop->viscosity(*P, *h);
			
			Re = abs(Re);
			if (Re < 1.0)
				Re = 1.0;
			double Pr = prop->Pr(*P, *h);
			Pr = abs(Pr);
			switch (PipeGeometry) {
			case roundPipe:
				if (*metal)
					Nu = pipe::round::metal::Nu(Re * Pr);
				else
					if (prop->is_it_saturation(*P, *h) && Tw > * T && D > 0.01) {
						Re = abs(*G0) / *s * D / prop->viscos_liq(*P);
						Pr = prop->Pr_liq(*P);
						double Re_vap = abs(*G0) / *s * D / prop->viscos_vap(*P);
						double Pr_vap = prop->Pr_vap(*P);
						double alph = pipe::round::boiling::alpha(Re, Pr, *P, *h, *T, Tw, *G0, *s, D, prop, Re_vap, Pr_vap);
						double alph0 = pipe::round::Nu(Re, Pr)*prop->thermalConductivity(*P, *h) / D;
						double x = prop->x(*P, *h);
						if (x < 0.1)
							return (alph*x + alph0*(0.1 - x))/(0.1);
						if (x > 0.9)
							return (alph*(1-x) + alph0 * (x-0.9)) / (0.1);
						return pipe::round::boiling::alpha(Re, Pr, *P, *h, *T, Tw, *G0, *s, D, prop, Re_vap, Pr_vap);
					}
					else
						Nu = pipe::round::Nu(Re, Pr);
				break;

			case ringPipe:
				/*if (*metal)*/
					Nu = pipe::ring::metall::Nu(Re, Pr, tet);
					//Nu = pipe::ring::Nu(Re, Pr, tet);
				break;

			case puchok:
				D = Dvn * (2 * sqrt(3)* *shag* *shag / pi - 1);
				Re = abs(*G) / *s * D / abs(prop->viscosity(*P, *h));
				Nu = pucki::Nu(Re * Pr, *shag, *phi);				
				break;
			}

			return Nu * prop->thermalConductivity(*P, *h) / D;
		}
	};
	wallGeometry WallGeometry;
	PropertiesW *Prop;
	Cell A, B;
	vector<double> T;
	vector<double> T_;
	vector<double> m;
	vector<double> S1;
	vector<double> S2;
	vector<double> dr;
	vector<double> Rt1;
	vector<double> Rt2;
	vector<double> r;
	vector<double> r1;
	vector<double> r2;
	bool provdvumerA = false;
	bool provdvumerB = false;	
	double Rt;
	HeatStructure(PropertiesW *prop, Channel &A_channel, unsigned A_i, Channel &B_channel, unsigned B_i, unsigned n) {
		T.resize(n);
		T_.resize(n);
		m.resize(n);
		S1.resize(n);
		S2.resize(n);
		dr.resize(n);
		Rt1.resize(n);
		Rt2.resize(n);
		r.resize(n);
		r1.resize(n);
		r2.resize(n);
		
		A_channel.geom[A_i].Twall = &T[n-1];
		B_channel.geom[B_i].Twall = &T[n-1];

		WallGeometry = cylindrical;
		Prop = prop;

		if (A_channel.geom[A_i].Dvn)
			B.Dvn = A_channel.geom[A_i].Dvn;

		A.Q = &A_channel.Q[A_i];
		A.G = &A_channel.G2[A_i];
		A.G0 = A_channel.G0;
		A.h = &A_channel.h[A_i];
		A.P = &A_channel.P[A_i];
		A.s = &A_channel.geom[A_i].s;
		A.prop = &A_channel.properties;
		A.phi = &A_channel.geom[A_i].phi;
		A.shag = &A_channel.shag;
		A.T = &A_channel.T[A_i];
		
		A.melt = &A_channel.melt[A_i];

		B.Q = &B_channel.Q[B_i];
		B.G = &B_channel.G2[B_i];
		B.G0 = B_channel.G0;
		B.h = &B_channel.h[B_i];
		B.P = &B_channel.P[B_i];
		B.s = &B_channel.geom[B_i].s;
		B.prop = &B_channel.properties;
		B.phi = &B_channel.geom[B_i].phi;
		B.shag = &B_channel.shag;
		B.melt = &B_channel.melt[B_i];
		B.T = &B_channel.T[B_i];
	
		A.metal = &A_channel.IsItMetal;
		B.metal = &B_channel.IsItMetal;
		if(A_channel.geom[A_i].d1==0 && A_channel.geom[A_i].d2 > 0)
			A.PipeGeometry = roundPipe;
		if(A_channel.geom[A_i].d2 > A_channel.geom[A_i].d1 && A_channel.geom[A_i].d1>0)
			A.PipeGeometry = ringPipe;
		if (A_channel.shag)
			A.PipeGeometry = puchok;

		if (B_channel.geom[B_i].d1 == 0 && B_channel.geom[B_i].d2 > 0)
			B.PipeGeometry = roundPipe;
		if (B_channel.geom[B_i].d2 > B_channel.geom[B_i].d1 && B_channel.geom[B_i].d1 > 0)
			B.PipeGeometry = ringPipe;
		if (B_channel.shag)
			B.PipeGeometry = puchok;

		double dy = A_channel.geom[A_i].dx;
		double d2 = B_channel.geom[B_i].d1;
		if (B.PipeGeometry == puchok)
			d2 = A_channel.geom[A_i].Dvn;
		double d1 = A_channel.geom[A_i].d2;
		double dx = (d2 - d1) / (T.size() - 1) / 2;

		A.D = A_channel.geom[A_i].d2 - A_channel.geom[A_i].d1;
		B.D = B_channel.geom[B_i].d2 - B_channel.geom[B_i].d1;
		A.tet = A_channel.geom[A_i].d1 / A_channel.geom[A_i].d2;
		B.tet = B_channel.geom[B_i].d1 / B_channel.geom[B_i].d2;

		for (auto &d : dr)
			d = dx;

		dr[0] *= 0.5;
		dr[dr.size() - 1] *= 0.5;

		r1[0] = d1 / 2.0;
		r2[0] = r1[0] + dr[0];
		r[0] = r1[0];
		S1[0] = 2.0*pi*r1[0] * dy;
		S2[0] = 2.0*pi*r2[0] * dy;

		double T1 = *A.T;
		double T2 = *B.T;
		T[0] = T1;
		for (int i = 1; i < r1.size(); i++) {
			r1[i] = r2[i - 1];
			r2[i] = r1[i] + dr[i];
			r[i] = (r1[i] + r2[i]) / 2.0;
			S1[i] = 2.0*pi*r1[i] * dy;
			S2[i] = 2.0*pi*r2[i] * dy;
			T[i] = T1 + (T2 - T1) / log(d2 / d1)*log(2 * r[i] / d1);

		}
		r[r.size() - 1] = r2[r2.size() - 1];
		for (int i = 0; i < r1.size(); i++)
			m[i] = pi * dy*dr[i] * (r1[i] + r2[i])*Prop->density(T[i]) / Time::step;
		T[T.size() - 1] = T2;
		for (int i = 0; i < T.size(); i++) {
			T_[i] = T[i];
		}

		for (int i = 0; i < T.size() - 1; i++)
			Rt2[i] = S2[i] / r2[i] / log(r[i + 1] / r[i]);

		for (int i = 1; i < T.size(); i++) {
			Rt1[i] = S1[i] / r1[i] / log(r[i] / r[i - 1]);
		}
		Rt1[0] = 0.0;
		Rt2[Rt2.size() - 1] = 0.0;
	}
	void Q0() {
		*A.Q = 0.0;
		*B.Q = 0.0;
	}
	void calc() {
		
		double Q1 = 0.0;
		double Q2 = 0.0;
		for (int it = 0; it < 5; it++) {
			double alpha1 = A.alpha(T[0]);
			double alpha2 = B.alpha(T[T.size() - 1]);

			int i = 0;
			double cp = Prop->heatCapacity(T[i]);
			double R1 = alpha1 * S1[i];
			double R2 = Rt2[i] * Prop->thermalConductivity((T[i] + T[i + 1]) / 2);
			T[i] = (m[i] * cp * T_[i] + R1 * *A.T + R2 * T[i + 1]) / (m[i] * cp + R1 + R2);

			for (i = 1; i < T.size() - 1; i++) {
				cp = Prop->heatCapacity(T_[i]);
				R1 = Rt1[i] * Prop->thermalConductivity((T[i] + T[i - 1]) / 2);
				R2 = Rt2[i] * Prop->thermalConductivity((T[i] + T[i + 1]) / 2);
				T[i] = (m[i] * cp * T_[i] + R1 * T[i - 1] + R2 * T[i + 1]) / (m[i] * cp + R1 + R2);
			}

			i = (int)T.size() - 1;
			cp = Prop->heatCapacity(T[i]);
			R1 = Rt1[i] * Prop->thermalConductivity((T[i] + T[i - 1]) / 2);
			R2 = alpha2 * S2[i];

			if (*(B.melt))
				T[i] = (m[i] * cp * T_[i] + R1 * T[i - 1] + (*(B.melt))->Qsumrad) / (m[i] * cp + R1);
			else
				T[i] = (m[i] * cp * T_[i] + R1 * T[i - 1] + R2 * *B.T) / (m[i] * cp + R1 + R2);

			Q1 = S1[0] * alpha1 * (T[0] - *A.T);	

			Q2 = S2[S2.size() - 1] * alpha2 * (T[T.size() - 1] - *B.T);
	
		}		

		*A.Q += Q1;
		*B.Q += Q2;

		for (int i = 0; i < T.size(); i++) {
			T_[i] = T[i];
		}
	}

};

